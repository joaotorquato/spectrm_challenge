# Spectrm Challenge

Intructions:

> Using any javascript framework, create a carousel (max 10 images) of GIFs using Giphy's public API for random gifs: [GIPHY DOCS](https://developers.giphy.com/docs/)

> The carousel should preview all the GIFs using the "downsized_small" version and only load gifs up to 100KB in size.

> The requests to Giphy's API should be done in a simple backend provided by any objected oriented language you are familiar with.

> Please provide minimal instructions on how to run the project and deliver it in a compressed archive.

## Dependencies

To run this project you need to have:

* [Docker compose](https://docs.docker.com/compose/install/)

## Setup the project

  1. Install the dependencies above
  2. `$ docker-compose build --no-cache` - Build docker images
  3. `$ docker-compose up` - Create and start containers

  If everything goes OK, you can now access the application!

## Run the application

  1. Open [http://localhost:3000](http://localhost:3000)

## Running automated tests

  1. `$ docker-compose run --rm web bash`
  2. `$ yarn test` to run the javascript specs
  3. `$ rspec` to run the rails specs
