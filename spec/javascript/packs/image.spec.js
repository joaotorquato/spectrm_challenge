import React from 'react'
import { shallow } from 'enzyme'
import Image from 'packs/image'

describe('Image component', () => {
  beforeEach(() => {
    fetch.mockResponse(JSON.stringify({
      "id":"l0NwLis8QAjk4RA6A",
      "title":"tongue lick GIF by Justin Gammon",
      "number_of_tries":2,
      "downsized_small":"https://media2.giphy.com/media/l0NwLis8QAjk4RA6A/giphy-downsized-small.mp4",
      "original":"https://media2.giphy.com/media/l0NwLis8QAjk4RA6A/giphy.mp4",
      "original_size":"26713"
    }));
  });

  it('should loading a image before the ajax', () => {
    const wrapper = shallow(<Image />);

    expect(wrapper.find('img').length).toEqual(1);
    expect(wrapper.find('video').length).toEqual(0);
  })

  it('should loading a video after the ajax', async () => {
    const wrapper = shallow(<Image />);

    await wrapper.instance().componentDidMount();
    wrapper.update()

    expect(wrapper.find('img').length).toEqual(0);
    expect(wrapper.find('video').length).toEqual(1);
  })
})
