import React from 'react'
import { shallow, mount } from 'enzyme'
import Carousel from 'packs/carousel'
import Image from 'packs/image'

describe('Carousel component', () => {
  it('should render 10 <Image /> components', () => {
    const wrapper = shallow(<Carousel />);

    expect(wrapper.find(Image).length).toEqual(10);
  })

  it('when child is clicked should enlarge the image', async () => {
    fetch.mockResponse(JSON.stringify({
      "id":"l0NwLis8QAjk4RA6A",
      "title":"tongue lick GIF by Justin Gammon",
      "number_of_tries":2,
      "downsized_small":"https://media2.giphy.com/media/l0NwLis8QAjk4RA6A/giphy-downsized-small.mp4",
      "original":"https://media2.giphy.com/media/l0NwLis8QAjk4RA6A/giphy.mp4",
      "original_size":"26713"
    }));
    const wrapper = mount(<Carousel />);

    expect(wrapper.find('.gif-enlarged').length).toEqual(0);

    await wrapper.find(Image).first().instance().componentDidMount();
    wrapper.update();
    wrapper.find('video').first().simulate('click')

    expect(wrapper.find('.gif-enlarged').length).toEqual(1);
  })
})
