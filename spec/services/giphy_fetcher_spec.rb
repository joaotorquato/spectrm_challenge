require 'rails_helper'

RSpec.describe GiphyFetcher, type: :service do
  describe '.random', vcr: { record: :new_episodes } do
    it 'returns a random gif from the giphy API' do
      result = GiphyFetcher.random

      expect(result).to eq(
        id: '3oEduFxDwViqIToNPO',
        title: 'deep in my heart GIF by Warner Archive',
        downsized_small: 'https://media0.giphy.com/media/3oEduFxDwViqIToNPO/giphy-downsized-small.mp4',
        original: 'https://media0.giphy.com/media/3oEduFxDwViqIToNPO/giphy.mp4',
        original_size: '73992',
        number_of_tries: 1
     )
    end

    context 'when is an image of more than 100kb' do
      it 'requests from the API till gets a image less than 100kb' do
        result = GiphyFetcher.random

        expect(result).to eq(
          id: '3o85fR0Ar4BCEeoNxu',
          title: 'ilana glazer GIF by Broad City',
          downsized_small: 'https://media3.giphy.com/media/3o85fR0Ar4BCEeoNxu/giphy-downsized-small.mp4',
          original: 'https://media3.giphy.com/media/3o85fR0Ar4BCEeoNxu/giphy.mp4',
          original_size: '42664',
          number_of_tries: 6
        )
      end
    end
  end
end
