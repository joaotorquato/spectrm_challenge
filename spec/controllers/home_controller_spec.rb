require 'rails_helper'

RSpec.describe HomeController, type: :controller do
  describe 'GET #index' do
    before { get :index  }
    it { expect(response).to have_http_status(:success) }
  end

  describe 'GET #random_gif', :vcr do
    it 'returns a random gif from the giphy API' do
      expect(GiphyFetcher).to receive(:random)

      get :random_gif, as: :json
    end
  end
end
