Rails.application.routes.draw do
  root to: 'home#index'
  get :random_gif, to: 'home#random_gif'
end
