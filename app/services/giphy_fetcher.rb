class GiphyFetcher
  GIPHY_URL = 'http://api.giphy.com/v1/gifs/random'.freeze
  GIPHY_TOKEN = 'MTgZSty8dsOggm3e91NZ6vuSsUwvSSgk'.freeze
  MAXIMUM_SIZE = 102_400 # 100kb

  def self.random
    new.fetch
  end

  def fetch
    result = { original_size: 10_000_000_000_000 }
    number_of_tries = 0
    while image_size_too_big?(result)
      number_of_tries += 1
      url = GIPHY_URL + "?api_key=#{GIPHY_TOKEN}"
      resp = Net::HTTP.get_response(URI.parse(url))
      result = format_response(JSON.parse(resp.body), number_of_tries)
    end
    result
  end

  private

  def image_size_too_big?(result)
    result[:original_size].to_i > MAXIMUM_SIZE
  end

  def format_response(response_json, number_of_tries)
    {
      id: response_json['data']['id'],
      title: response_json['data']['title'],
      number_of_tries: number_of_tries
    }.merge(images_hash(response_json))
  end

  def images_hash(response_json)
    images = response_json['data']['images']
    {
      downsized_small: images['downsized_small']['mp4'],
      original: images['original']['mp4'],
      original_size: images['original']['mp4_size']
    }
  end
end
