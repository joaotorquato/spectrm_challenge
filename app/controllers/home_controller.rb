class HomeController < ApplicationController
  def random_gif
    @gif = GiphyFetcher.random

    respond_to do |format|
      format.json { render json: @gif }
    end
  end
end
