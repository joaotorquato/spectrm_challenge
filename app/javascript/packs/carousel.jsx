import React from 'react'
import Image from 'packs/image'

class Carousel extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      src: '',
      alt: ''
    }
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(image) {
    this.setState({
      src: image.state.original,
      alt: image.state.alt
    });
  }

  enlargeGif = () => {
    if(this.state.src != ''){
      return (<video src={this.state.src}
                  className="gif-enlarged img-thumbnail"
                  autoPlay loop muted
                  alt={this.state.alt}
                  style={{height: '330px', width: '770px'}}/>);
    }
    return <h1 className="text-center img-thumbnail" style={{height: '330px', width: '770px'}}> Click on gif to enlarge</h1>;
  }

  addImages = () => {
    let images = [...Array(10).keys()].map((key) => {
      return <Image key={key.toString()} handleClick={this.handleClick} />;
    });

    return images;
  }

  render() {
    return (
     <React.Fragment>
      <div className="row justify-content-md-center">
        <div className="col-md-auto">
          {this.enlargeGif()}
        </div>
      </div>
      <div className="row justify-content-md-center">
        {this.addImages()}
      </div>
     </React.Fragment>
    )
  }
}

export default Carousel;
