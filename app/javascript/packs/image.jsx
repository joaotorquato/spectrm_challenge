import React from 'react'
import request from 'request';
import 'babel-polyfill';

class Image extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
        src_img: 'https://zippy.gfycat.com/SkinnySeveralAsianlion.gif',
        src: '', original: '', alt: 'Loading', id: this.key
    };
  }

  async componentDidMount() {
    var that = this;
    const response = await fetch('http://localhost:3000/random_gif.json');
    const giphy = await response.json();
    that.setState({src: giphy.downsized_small,
                   original: giphy.original,
                   alt: giphy.title,
                   id: giphy.id});
  }

  addLoading = () => {
    if(this.state.src == ''){
      return (<img style={{height: '150px', width: '150px'}} src={this.state.src_img} className="img-thumbnail"/>);
    }
    return;
  }

  addGif = () => {
    if(this.state.src != ''){
      return (<video src={this.state.src}
                  autoPlay loop muted
                  data-id={this.state.id}
                  original={this.state.original}
                  alt={this.state.alt}
                  onClick={() => this.props.handleClick(this)}
                  className="img-thumbnail"
                  style={{height: '150px', width: '150px', cursor: 'pointer'}}/>);
    }
    return;
  }

  render() {
    return (
      <div>
        {this.addLoading()}
        {this.addGif()}
      </div>
    );
  }
}

export default Image;
