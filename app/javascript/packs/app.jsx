import React from 'react'
import { render } from 'react-dom'
import Carousel from 'packs/carousel'

render(<Carousel />, document.querySelector('#main'));
